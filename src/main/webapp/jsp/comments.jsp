<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.test.blog.model.CommentItem" %>
<%@ page import="java.util.List" %>

<html>
    <body>
     <form action="/add-comment" method="post">
        <input type="text" name="comment" placeholder="Comment"/>
        <button type="submit">Add comment</button>
    </form>

    Posts: </br>
    <% for (CommentItemItem comment: (List<CommentItem>)request.getAttribute("comments")) { %>
        <form style="display: inline-block" method="post">
            <input type="hidden" name="number" value="<%= comments.getNumber() %>"/>
            <div><%= comments.getComment() %> </div>
        </form>
        <br/>
    <% } %>
    </body>
</html>