<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.test.blog.model.PostItem" %>
<%@ page import="java.util.List" %>

<html>
    <body>
     <form action="/add-post" method="post">
        <input type="text" name="title" placeholder="Title"/></br>
        <input type="text" name="post" placeholder="Post"/>
        <button type="submit">Add post</button>
    </form>

    Posts: </br>
    Title Date UserId</br>
    <% for (PostItem post: (List<PostItem>)request.getAttribute("posts")) { %>
        <form style="display: inline-block" action="/fullscreen-post" method="post">
            <input type="hidden" name="number" value="<%= post.getNumber() %>"/>
            <a href="/comments"><div><%= post.getTitle() %> <%= post.getDate() %> <%= post.getUserID() %> </div></a>
        </form>
        <br/>
    <% } %>
    </body>
</html>