package com.test.blog.model;

import java.util.Date;

/**
 * Created by Valeriy on 16.01.2017.
 */
public class CommentItem {
    private long number;
    private String comment;
    private Date date;
    private Long userID;

    public CommentItem(long number, String comment,Date date,Long userID) {
        this.number = number;
        this.date = date;
        this.userID=userID;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public Long getUserID() {
        return userID;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return (int) (number ^ (number >>> 32));
    }
}
