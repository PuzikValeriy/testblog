package com.test.blog.model;

import java.util.Date;

/**
 * Created by java on 16.01.2017.
 */
public class PostItem {

    private long number;

    private String title;
    private String post;
    private Date date;
    private Long userID;

    public PostItem(long number, String title,String post,Date date,Long userID) {
        this.title=title;
        this.number = number;
        this.post = post;
        this.date = date;
        this.userID=userID;
    }

    public String getTitle() {
        return title;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public Long getUserID() {
        return userID;
    }
    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PostItem postItem = (PostItem) o;

        return number == postItem.number;

    }

    @Override
    public int hashCode() {
        return (int) (number ^ (number >>> 32));
    }
}
