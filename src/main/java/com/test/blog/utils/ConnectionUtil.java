package com.test.blog.utils;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Valeriy on 16.01.2017.
 */
public class ConnectionUtil {
    public static Connection createConnection() {
        Driver driver = new org.postgresql.Driver();
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection("jdbc:postgresql://localhost:5432/blog", "postgres", "7492563");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
