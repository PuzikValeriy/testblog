package com.test.blog.controller;

import com.test.blog.model.CommentItem;
import com.test.blog.utils.ConnectionUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Valeriy on 16.01.2017.
 */
public class CommentsServlet extends HttpServlet{
    private List<CommentItem> comments = Collections.synchronizedList(new ArrayList<>());

    @Override
    public void init() throws ServletException {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<CommentItem> comments = this.comments.stream()
                .collect(Collectors.toList());
        req.setAttribute("comments", comments);
        try(Connection connection = ConnectionUtil.createConnection()){
            if(connection!=null){
                PreparedStatement statement = connection.prepareStatement("SELECT * FROM comments");
                ResultSet resultSet =statement.executeQuery();
                while(resultSet.next()){
                    comments.add(new CommentItem(resultSet.getLong("id"),resultSet.getString("comment"),resultSet.getDate("date"),resultSet.getLong("user_id")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.getRequestDispatcher("/jsp/comments.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        switch (req.getRequestURI()) {
            case "/add-comment":
                String comment = req.getParameter("comment");
                long maxNumber = Collections.max(comments.stream().map(CommentItem::getNumber).collect(Collectors.toList()));
                comments.add(new CommentItem(maxNumber + 1, comment, new java.util.Date(),(Long) req.getSession().getAttribute("userid")));
                try (Connection connection = ConnectionUtil.createConnection()) {
                    if (connection != null) {
                        PreparedStatement statement = connection.prepareStatement("INSERT INTO comments VALUES(?,?,?,?)");
                        statement.setLong(1, maxNumber + 1);
                        statement.setString(2, comment);
                        statement.setDate(3, Date.valueOf(LocalDate.now()));
                        statement.setLong(4, (Long) req.getSession().getAttribute("userid"));
                        statement.executeUpdate();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
        }
        resp.sendRedirect("/comments");
    }
}
