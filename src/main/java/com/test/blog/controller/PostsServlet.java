package com.test.blog.controller;

import com.test.blog.model.PostItem;
import com.test.blog.utils.ConnectionUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class PostsServlet extends HttpServlet {

    private List<PostItem> posts = Collections.synchronizedList(new ArrayList<>());

    @Override
    public void init() throws ServletException {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<PostItem> posts = this.posts.stream()
                .collect(Collectors.toList());
        req.setAttribute("posts", posts);
        try(Connection connection = ConnectionUtil.createConnection()){
            if(connection!=null){
                PreparedStatement statement = connection.prepareStatement("SELECT * FROM posts");
                ResultSet resultSet =statement.executeQuery();
                while(resultSet.next()){
                    posts.add(new PostItem(resultSet.getLong("id"),resultSet.getString("title"),resultSet.getString("post"),resultSet.getDate("date"),resultSet.getLong("user_id")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.getRequestDispatcher("/jsp/posts.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        switch (req.getRequestURI()) {
            case "/add-post":
                String title = req.getParameter("title");
                String post = req.getParameter("post");
                long maxNumber = Collections.max(posts.stream().map(PostItem::getNumber).collect(Collectors.toList()));
                posts.add(new PostItem(maxNumber + 1, title, post, new java.util.Date(), (Long) req.getSession().getAttribute("userid")));
                try (Connection connection = ConnectionUtil.createConnection()) {
                    if (connection != null) {
                        PreparedStatement statement = connection.prepareStatement("INSERT INTO posts VALUES(?,?,?,?,?)");
                        statement.setLong(1, maxNumber + 1);
                        statement.setString(2, title);
                        statement.setDate(3, Date.valueOf(LocalDate.now()));
                        statement.setLong(4, (Long) req.getSession().getAttribute("userid"));
                        statement.setString(5, post);
                        statement.executeUpdate();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case "/fullscreen-post":
                //req.getSession().setAttribute("postID",2);
                resp.sendRedirect("/comments");
                break;
                }
                resp.sendRedirect("/posts");
        }
    }
