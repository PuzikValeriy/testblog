package com.test.blog.controller;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Valeriy on 16.01.2017.
 */
public class LoginFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpReq = (HttpServletRequest) servletRequest;
        HttpSession session = httpReq.getSession(true);

        if(session.getAttribute("userid")!=null){
            filterChain.doFilter(servletRequest,servletResponse);
        } else {
            HttpServletResponse httpResp = (HttpServletResponse) servletResponse;
            httpResp.sendRedirect("/login");
        }
    }

    @Override
    public void destroy() {

    }
}
