package com.test.blog.controller;

import com.test.blog.utils.ConnectionUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Valeriy on 16.01.2017.
 */
public class LoginServlet extends HttpServlet{
    private static  long idDB;
    private static  String loginDB;
    private static  String passwordDB;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/jsp/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

            try(Connection connection = ConnectionUtil.createConnection()){
                if(connection!=null){
                    PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE login=? AND password=?");
                    statement.setString(1,login);
                    statement.setString(2,password);
                    ResultSet resultSet =statement.executeQuery();
                    if(resultSet.next()){
                        idDB=resultSet.getLong("id");
                        loginDB=resultSet.getString("login");
                        passwordDB=resultSet.getString("password");
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        if(login.equals(loginDB)&&password.equals(passwordDB)){
            HttpSession session = req.getSession(true);
            session.setAttribute("login",loginDB);
            session.setAttribute("userid",idDB);
            resp.sendRedirect("/posts");
        } else {
            resp.sendRedirect("/login");
        }
    }
}
