package com.test.blog.controller;

import com.test.blog.utils.ConnectionUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;

/**
 * Created by Valeriy on 15.01.2017.
 */
public class RegistrationServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/jsp/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        int insertedRows=0;

            try(Connection connection = ConnectionUtil.createConnection()){
                if(connection!=null){
                    PreparedStatement statement = connection.prepareStatement("INSERT INTO users (login,password) VALUES (?,?)");
                    statement.setString(1,login);
                    statement.setString(2,password);

                    insertedRows = statement.executeUpdate();
                    if(insertedRows==0){
                        throw new SQLException("no rows inserted");
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        if(insertedRows!=0){
            HttpSession session = req.getSession(true);
            resp.sendRedirect("/login");
        } else {
            resp.sendRedirect("/registration");
        }
    }
}
